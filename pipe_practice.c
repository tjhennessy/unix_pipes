#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <poll.h>

#define READ 0
#define WRITE 1

static int BUFF_SZ;

typedef struct thread_ctx
{
    int read_end_fd;
} thread_ctx_t;

void err(char *msg)
{
    fprintf(stderr, "Error: %s\n", msg);
    pthread_exit(NULL);
}

void closefd(void *arg)
{
    int *fd = (int *)arg;
    close(*fd);
}

void free_string(char *str)
{
    if (str != NULL)
    {
        free(str);
    }
}

void *entry_function(void *ctx)
{
    thread_ctx_t *thread_ctx = (thread_ctx_t *)ctx;
    struct pollfd fd;
    nfds_t nfds = 1;
    int rv;
    int num_bytes_read;

    pthread_cleanup_push(closefd, (void *)&(thread_ctx->read_end_fd));
    pthread_cleanup_push(free, (void *) buf);

    fd.fd = thread_ctx->read_end_fd;
    fd.events = POLLIN;

    for (;;)
    {
        rv = poll(&fd, nfds, -1); /* cancellation point */
        if (rv > 0 && fd.fd == thread_ctx->read_end_fd && fd.revents & POLLIN)
        {
            /* Read data from pipe, echo on stdout */
            buf = calloc(BUFF_SZ + 1, sizeof(char));
            if (buf == NULL)
            {
                err("calloc failed.");
            }
            num_bytes_read = read(fd.fd, buf, BUFF_SZ);
            if (num_bytes_read == -1)
                err("read failed");
            if (num_bytes_read == 0)
                break; /* End-of-file */
            if (write(STDOUT_FILENO, buf, num_bytes_read) != num_bytes_read)
            {
                err("child - partial/failed write");
            }
            write(STDOUT_FILENO, "\n", 1);
            break;
        }
        else
        { /* rv 0 -> timeout (not poss if timeout==-1), rv -1 -> some error */
            /* could also be here b/c of another revent type such as POLLHUP */
            break;
        }

        pthread_testcancel(); /* cancellation point */
    }

    pthread_cleanup_pop(1);
    pthread_cleanup_pop(1);
    pthread_exit(NULL); /* Will automatically pop cleanup stack */
}

int main(void)
{
    int pipefd[2];
    int rv;
    thread_ctx_t ctx;
    pthread_t tid;
    char msg[] = "Test of the emergency pipe system!";

    rv = pipe(pipefd);
    if (-1 == rv)
    {
        perror("call to pipe failed");
        return 1;
    }
    ctx.read_end_fd = pipefd[READ]; /* will be closed by thread */

    rv = pthread_create(&tid, NULL, &entry_function, &ctx);
    if (rv != 0)
    {
        err("could not create thread.");
    }

    usleep(3 * 1000000); /* three seconds */
    //pthread_cancel(tid);
    BUFF_SZ = strlen(msg);
    if (write(pipefd[WRITE], msg, BUFF_SZ) != BUFF_SZ)
    {
        err("parent - partial/failed write");
    }

    rv = pthread_join(tid, NULL);
    if (rv != 0)
    {
        err("could not join with thread.");
    }

    close(pipefd[WRITE]);

    pthread_exit(NULL);
}
